using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Standarddialoge_GUI
{
    public partial class Standarddialoge : Form
    {
        public Standarddialoge()
        {
            InitializeComponent();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();      // Instanzieren eines "Datei öffnen" Dialogs
            ofd.InitialDirectory = "C:\\";                  // Startordner festlegen
            ofd.Filter = "Alle Dateien (*.*)|*.*";          // Dateitypauswahl festlegen
            ofd.Multiselect = true;                         // Mehrfachauswahl möglich
            ofd.Title = "Datei(en) zum öffnen auswählen";   // Dialogtitel
            if (ofd.ShowDialog() == DialogResult.OK)
                foreach (string str in ofd.FileNames)
                    MessageBox.Show("Öffnen: " + str);
            else
                MessageBox.Show("Abbruch");
        }

        private void cmd_save_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();      // Instanzieren eines "Datei speichern" Dialogs
            sfd.InitialDirectory = "C:\\";
            sfd.Filter = "Textdateien (*.txt*|*.txt|" + "C# Quellcode (*.cs)|*.cs*|" + "Alle Dateien (*.*)|*.*";
            sfd.Title = "Dateinamen zum speichern eingeben";
            if (sfd.ShowDialog() == DialogResult.OK)
                MessageBox.Show("Speichern unter: " + sfd.FileName);
            else
                MessageBox.Show("Abbruch");
        }

        private void cmd_Browser_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();    // Instanzieren des "Folderbrowser" Dialogs
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            fbd.ShowNewFolderButton = false;
            if (fbd.ShowDialog() == DialogResult.OK)
                MessageBox.Show("Verzeichniss: " + fbd.SelectedPath);
            else
                MessageBox.Show("Abbruch");
        }

        private void cmd_Font_Click(object sender, EventArgs e)
        {
            FontDialog fd = new FontDialog();
            if (fd.ShowDialog() == DialogResult.OK)
                lbl_Text.Font = fd.Font;
            else
                MessageBox.Show("Abbruch");
        }

        private void cmd_color_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
            {
                lbl_Text.ForeColor = cd.Color;
                // lbl_Text.BackColor = cd.Color;
            }
        }
    }
}
