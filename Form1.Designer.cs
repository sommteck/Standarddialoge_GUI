namespace Standarddialoge_GUI
{
    partial class Standarddialoge
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmd_open = new System.Windows.Forms.Button();
            this.cmd_save = new System.Windows.Forms.Button();
            this.cmd_Browser = new System.Windows.Forms.Button();
            this.cmd_Font = new System.Windows.Forms.Button();
            this.cmd_color = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.lbl_Text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cmd_open
            // 
            this.cmd_open.Location = new System.Drawing.Point(33, 84);
            this.cmd_open.Name = "cmd_open";
            this.cmd_open.Size = new System.Drawing.Size(88, 52);
            this.cmd_open.TabIndex = 0;
            this.cmd_open.Text = "&Open File Dialog";
            this.cmd_open.UseVisualStyleBackColor = true;
            this.cmd_open.Click += new System.EventHandler(this.cmd_open_Click);
            // 
            // cmd_save
            // 
            this.cmd_save.Location = new System.Drawing.Point(33, 160);
            this.cmd_save.Name = "cmd_save";
            this.cmd_save.Size = new System.Drawing.Size(88, 58);
            this.cmd_save.TabIndex = 1;
            this.cmd_save.Text = "&Save File Dialog";
            this.cmd_save.UseVisualStyleBackColor = true;
            this.cmd_save.Click += new System.EventHandler(this.cmd_save_Click);
            // 
            // cmd_Browser
            // 
            this.cmd_Browser.Location = new System.Drawing.Point(33, 246);
            this.cmd_Browser.Name = "cmd_Browser";
            this.cmd_Browser.Size = new System.Drawing.Size(88, 51);
            this.cmd_Browser.TabIndex = 2;
            this.cmd_Browser.Text = "&Folder Browser Dialog";
            this.cmd_Browser.UseVisualStyleBackColor = true;
            this.cmd_Browser.Click += new System.EventHandler(this.cmd_Browser_Click);
            // 
            // cmd_Font
            // 
            this.cmd_Font.Location = new System.Drawing.Point(156, 84);
            this.cmd_Font.Name = "cmd_Font";
            this.cmd_Font.Size = new System.Drawing.Size(91, 52);
            this.cmd_Font.TabIndex = 3;
            this.cmd_Font.Text = "&Font Dialog";
            this.cmd_Font.UseVisualStyleBackColor = true;
            this.cmd_Font.Click += new System.EventHandler(this.cmd_Font_Click);
            // 
            // cmd_color
            // 
            this.cmd_color.Location = new System.Drawing.Point(156, 160);
            this.cmd_color.Name = "cmd_color";
            this.cmd_color.Size = new System.Drawing.Size(91, 58);
            this.cmd_color.TabIndex = 4;
            this.cmd_color.Text = "Color Dialog";
            this.cmd_color.UseVisualStyleBackColor = true;
            this.cmd_color.Click += new System.EventHandler(this.cmd_color_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(156, 246);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(91, 51);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // lbl_Text
            // 
            this.lbl_Text.AutoSize = true;
            this.lbl_Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Text.Location = new System.Drawing.Point(79, 32);
            this.lbl_Text.Name = "lbl_Text";
            this.lbl_Text.Size = new System.Drawing.Size(134, 25);
            this.lbl_Text.TabIndex = 6;
            this.lbl_Text.Text = "Hello World";
            // 
            // Standarddialoge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 333);
            this.Controls.Add(this.lbl_Text);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_color);
            this.Controls.Add(this.cmd_Font);
            this.Controls.Add(this.cmd_Browser);
            this.Controls.Add(this.cmd_save);
            this.Controls.Add(this.cmd_open);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Standarddialoge";
            this.Text = "Standarddialoge";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cmd_open;
        private System.Windows.Forms.Button cmd_save;
        private System.Windows.Forms.Button cmd_Browser;
        private System.Windows.Forms.Button cmd_Font;
        private System.Windows.Forms.Button cmd_color;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Label lbl_Text;
    }
}

